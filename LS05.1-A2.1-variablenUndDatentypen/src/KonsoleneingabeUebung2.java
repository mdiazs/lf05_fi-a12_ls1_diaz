import java.util.Scanner;

public class KonsoleneingabeUebung2 {
	public static void main(String[] args) 
	  { 
		Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie Ihren Namen ein: ");
	    
	    String name = myScanner.next();
	   
	    System.out.print("Bitte geben Sie Ihre Alter ein: ");
	    
	    int alter = myScanner.nextInt();
	    
	    System.out.print("Hallo, " + name + "! Sie sind " + alter + " Jahre alt.");
	  }
}

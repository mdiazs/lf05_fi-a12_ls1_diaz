import java.util.Scanner;

public class programmierUebungenFuerMethoden {

		public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		// System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString("Was m�chten Sie bestellen? ");
		
		
		//System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");
		
			

		//System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		/* System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");*/
}
	public static String liesString(String artikelBezeichnung) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(artikelBezeichnung);
		String artikel = myScanner.next();
		return artikel;
	}
	public static int liesInt(String anzahlArtikeln) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(anzahlArtikeln);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	public static double liesDouble(String nettoPreiseingabe) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(nettoPreiseingabe);
		double preis = myScanner.nextDouble();
		return preis;
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, 
			double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechungAusgeben(String artikel, int anzahl, double
			nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
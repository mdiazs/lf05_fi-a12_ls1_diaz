﻿/* Welche Vorteile hat man durch diesen Schritt? (Auslagerung der Fahrkartenbezeichnungen und -preise in Arrays):
 * Man arbeitet mit fest definierten Daten. Neue Tickets und Korrekturen für Preise lassen sich einfach einfügen, falls nötig.
 * Die Elemente sind einem bestimmten Index zugeordnet. Dadurch lassen sich die einzelnen Daten eindeutig abrufen und man kann durch die Daten iterieren.
 * 
 * Vergleichen Sie die neue Implementierung mit der alten und erläutern Sie die Vor- und Nachteile der jeweiligen Implementierung.
 * Vorteile:
 * Die Bedienung wird für den Nutzer vereinfacht (alle Elemente können einfach mit einem Index abgerufen werden).
 * Es entsteht dadurch einer geringe Fehlermöglichkeit. Der Nutzer muss keine Preise manuell eingeben, die Preise lassen sich durch den entsprechenden Index abrufen.
 * Dadurch kann man die Nutzereingaben einfacher prüfen: man arbeitet nur mit einer festen Anzahl an Ticketbezeichnungen und Preisen. Alle Eingaben außerhalb dieser festen Anzahl ist keine gültige Eingabe.
 * 
 * Nachteile:
 * Man muss beachten, dass die Indexen von 0 bis n gehen. Die Nutzereingabe für die Ticketauswahl muss also um 1 dekrementiert werden.
 * Die Daten sind immernoch "hard-coded". Vertippt sich man im Code, können Fehler in der Preisberechnung entstehen. Stattdessen könnte man die Daten in einer Datenbank speichern und die Daten daraus ziehen. Korrekturen und Anpassungen ließen sich dadurch einfacher und sicherer tätigen, ohne den Programmcode abändern zu müssen.
 */



import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	// Nach Abwicklung eines Kaufvorgangs muss der Fahrkartenautomat wieder bereitstehen. Dies wird durch einen Boolean überprüft.
    	
    	boolean wiederholen = true;
    	
    	// Die do/while-Schleife lässt den Kaufvorgang mindestens einmal laufen und prüft nachher immer der Wert der Boolean-Variable.
    	
    	do {
         	
    	
       double zuZahlenderBetrag = fahrkartenbestellungErfassen(); 
           		
       double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag); 
       
       // Einfügen von Tickets und Geldeinwurf erfolgt im Methode fahrkartenbestellungErfassen()
       

       // Fahrscheinausgabe
       fahrkartenAusgeben();
       

       // Rückgeldberechnung und -Ausgabe
       
       rueckgeldAusgeben(rueckgabebetrag);
    	} while (wiederholen != false);
    	
      
    } // End main()
    public static double fahrkartenbestellungErfassen() {
    	
    	
    	
    	// Der Benutzer soll beim Kauf einer Fahrkarte zwischen verschiedenen Arten von Fahrkarten
    	Scanner tastatur = new Scanner(System.in);

    	boolean tarifErfasst = false;
    	    	
    	
    	// Dieses Array ordnen die Tickets einer Zahl zu, somit sind diese Zahlen wie "Tasten" für den Nutzer.
    	int auswahlArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    	
    	
    	String namenArray[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke",
    	                               "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
    	                               "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	
    	double preiseArray[] = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	double einzelpreis = 0.0;
    	
    	// Eine Do-While-Schleife prüft den Wert der Variable bestellungErfasst, die Schleife wird solange wiederholt, bis der Nutzer eine gültige Eingabe für die Fahrkarte getätigt hat.
    	
    	do {
 
    		System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin ABC aus:\n");
        	
    		for (int i = 0; i <= 9; i++) {
    			System.out.print(auswahlArray[i] + ". ");
    			System.out.print(namenArray[i] + " ");
    			System.out.print("(" + preiseArray[i] + " EUR).\n");
    		}
    	
    	int auswahl = tastatur.nextInt();
    	
    	// Eine Kontrollstruktur prüft die Auswahl auf Korrekheit. Gibt der Nutzer keine gültige Eingabe ein, wird eine Fehlermeldung ausgegeben.
    	if (auswahl > 0 && auswahl < 11) {
    		System.out.printf("Ihre Wahl: %d. %s\n\n", auswahlArray[auswahl - 1], namenArray[auswahl - 1]);
        	einzelpreis = preiseArray[auswahl - 1];
        	tarifErfasst = true;
    	}  else {
    		System.out.println("Ungültiger Auswahl.\n\n");
    	}
    	
    	} while (tarifErfasst != true);
    	
    	
    	
    	
    	
    	// Eine weitere Do-While-Schleife fragt nach der Anzahl der Tickets und wird solange wiederholt, bis es einen gültigen Wert eingegeben wird.
    	boolean anzahlErfasst = false;
    	int anzahlTickets = 0;
    	
    	double due = 0.0;
    	
    	do {
    		System.out.println("Wie viele Tickets möchten Sie kaufen?: ");
    		anzahlTickets = tastatur.nextInt();
    	
    	// Nur wenn die Eingabe korrekt ist (Zahl zwischen 1 und 10), wird der Wert von anzahlErfasst geändert und die Do-While-Schleife damit unterbrochen.	
    		if (anzahlTickets > 1 && anzahlTickets <= 10) {
    	// Zu zahlender Betrag wird berechnet		
    			due = anzahlTickets * einzelpreis;
    			anzahlErfasst = true;
    		} else {
    	// Bei ungültigen Eingaben wird eine Fehlermeldung ausgegeben.		
    			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
    		}
    		
    		
    	} while (anzahlErfasst != true);
    	
    	// Anzahl der Tickets wird ausgegeben, zu zahlender Betrag wird zurückgegeben in die Main-Methode.
    	System.out.printf("Anzahl der Tickets: %d\n\n", anzahlTickets);
        return due;
    	
        
        
        
        
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , zuZahlen - eingezahlterGesamtbetrag);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble(); 
            eingezahlterGesamtbetrag += eingeworfeneMünze; 
        }
        double payBack = eingezahlterGesamtbetrag - zuZahlen; 
        if(payBack > 0.0)
        {
     	   System.out.printf("Rückgabebetrag beträgt %.2f EURO\n" , payBack);           
        }
        return payBack;
    }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double moneyBack) {
    	if(moneyBack > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von  %.2f EURO\n" , moneyBack);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(moneyBack >= 2.0) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          moneyBack -= 2.0;
            }
            while(moneyBack >= 1.0) // 1 EURO-Münzen
            {
              muenzeAusgeben(1, "EURO");
 	          moneyBack -= 1.0;
            }
            while(moneyBack >= 0.5) // 50 CENT-Münzen
            {
              muenzeAusgeben(50, "EURO");
 	          moneyBack -= 0.5;
            }
            while(moneyBack >= 0.2) // 20 CENT-Münzen
            {
              muenzeAusgeben(20, "EURO");
  	          moneyBack -= 0.2;
            }
            while(moneyBack >= 0.1) // 10 CENT-Münzen
            {
              muenzeAusgeben(10, "EURO");
 	          moneyBack -= 0.1;
            }
            while(moneyBack >= 0.05)// 5 CENT-Münzen
            {
              muenzeAusgeben(5, "EURO");
  	          moneyBack -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    }
    public static void warte(int millisekunde) {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	if (betrag > 2) {
    		einheit = "CENT";
    	} 
    	System.out.println(betrag + " " + einheit);
    }
}
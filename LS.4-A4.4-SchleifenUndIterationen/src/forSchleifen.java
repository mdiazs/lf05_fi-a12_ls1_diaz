import java.util.Scanner;

public class forSchleifen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		// Aufgabe 1: Z�hlen
		
		// a.- heraufz�hlende Schleife
		
		Scanner input = new Scanner(System.in);
		System.out.println("Bis welcher Zahl m�chten Sie z�hlen? ");
		
		int ende = input.nextInt();
		
		for (int x = 1; x <= ende; ++x) {
			System.out.println(x);
		}
		
		// b.- herunterz�hlende Schleife
		
		for (int y = ende; y >= 1; --y) {
			System.out.println(y);
		}
	}

}

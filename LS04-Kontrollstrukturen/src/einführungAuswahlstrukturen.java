import java.util.Scanner;

public class einf�hrungAuswahlstrukturen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	// Aufgabe 1: Eigene Bedingungen
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Lass uns Lotto spielen. Gib zun�chst 2 Zahlen ein, zwischen 1 und 9.");
		System.out.println("Gib eine Zahl ein: ");
		int zahl01 = tastatur.nextInt();
		System.out.println("Gib eine zweite Zahl ein: ");
		int zahl02 = tastatur.nextInt();
		if (zahl01 >= zahl02) {
			System.out.println("Die erste Zahl ist gr��er oder gleich als die zweite.");
		} else if (zahl02 > zahl01) {
			System.out.println("Die zweite Zahl ist gr��er als die erste.");
		} else {
			System.out.println("Beide Zahlen sind gleich.");
		} 
		
	// Aufgabe 1: 
		
		System.out.println("Lass uns Lotto spielen. Gib zun�chst 3 Zahlen ein, zwischen 1 und 9.");
		System.out.println("Gib eine Zahl ein: ");
		int num01 = tastatur.nextInt();
		System.out.println("Gib eine zweite Zahl ein: ");
		int num02 = tastatur.nextInt();
		System.out.println("Gib eine dritte Zahl ein: ");
		int num03 = tastatur.nextInt();
		
		
	// Wenn die 1. Zahl gr��er als die 2. UND gr��er die 3. Zahl ist, soll eine Meldung ausgegeben werden.	
		if (num01 > num03 && num01 > num02) {
			System.out.println("Die erste Zahl har der gr��te Wert.");
		} 
		
	// Wenn die 3. Zahl gr��er als die 1. ODER gr��er als die 2. Zahl ist, soll eine Meldung ausgegeben werden.	
		else if (num03 > num02 || num03 > num01) {
			System.out.println("Die dritte Zahl ist entweder gr��er als die erste oder gr��e als die zweite.");
		}
	// Die gr��te der 3 Zahlen ausgeben.	
		if (num01 > num03 && num01 > num02) {
			System.out.println(num01);
		} else if (num02 > num01 && num02 > num03) {
			System.out.println(num02);
		} else {
			System.out.println(num03);
		}
		
		
		// Aufgabe 2:
		
		double nettopreis = 0.0;
		double bruttopreis = 0.0;
		double steuer = 0.0;
		double mwst_ermaessigt = 0.07;
		double mwst_voll = 0.19;
		String eingabe;
		
		System.out.println("Geben Sie den Nettopreis ein: ");
		nettopreis = tastatur.nextDouble();
		
		System.out.println("Welcher Steuersatz hat der Produkt? j f�r erm��igten Steuersatz, n f�r vollen Steuersatz");
		eingabe = tastatur.next();
		
		if (eingabe.equals("j") || eingabe.equals("J")) {
			steuer = nettopreis * mwst_ermaessigt;
		} else if (eingabe.equals("n") || eingabe.equals("N")) {
			steuer = nettopreis * mwst_voll;
		}
		
		bruttopreis = nettopreis + steuer;
		
		System.out.printf("Der Bruttopreis betr�gt: %.2f �.", bruttopreis);
		
		
		
		// Aufgabe 3:
		
		int bestellmenge = 0;
		double einzelpreis = 7.85;
		double lieferpreis;
		double rechnungsbetrag;
		double steuersatz;
		
		System.out.println("Wie viele M�use m�chten Sie bestellen? ");
		bestellmenge = tastatur.nextInt();
		
		if (bestellmenge >= 10) {
			lieferpreis = 0.0;
		} else if (bestellmenge < 10) {
			lieferpreis = 10.0;
		}
		
		steuersatz = (bestellmenge * einzelpreis) * 0.19;
		rechnungsbetrag = (bestellmenge * einzelpreis) + steuersatz;
		
		System.out.printf("Ihre Rechnung: %.2f �", rechnungsbetrag);
		
	
		
	}
}
